/*
 * single card
 */
public class Card
{
    String symbol;
    int value;
    
    public Card(int value, String symbol)
    {
        this.value = value;
        this.symbol = symbol;
    }

    public int getValue()
    {
        return value;
    }

    public void setValue(int value)
    {
        this.value = value;
    }

    public String getSymbol()
    {
        return symbol;
    }

    public void setSymbol(String symbol)
    {
        this.symbol = symbol;
    }
    
    public void print()
    {
    	System.out.println("Symbol: " + getSymbol());
    	System.out.println("Value: " + getValue());
    }
}
