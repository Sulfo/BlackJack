import java.util.ArrayList;
import java.util.Random;

public class Deck 
{
	private int[][] deck = {{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
            {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
            {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
            {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}};
	private ArrayList<Card> cardsInPlay = new ArrayList<Card>();
	
	public void AddCardsInPlay(Card card)
	{
		for (Card cardc : cardsInPlay)
		{
			if (cardc.getSymbol().equals(card.getSymbol()))
			{
				drawCard();
				break;
			}
		}
		cardsInPlay.add(card);
	}
	
	// draws from our deck
	public void drawCard()
    {
        Random MyRandom = new Random();
        int indexOne = MyRandom.nextInt(3);
        int indexTwo = MyRandom.nextInt(12);
        
        
        // Controle check?
        
        Card currentCard = convertToCard(deck[indexOne][indexTwo], indexOne);
        
        AddCardsInPlay(currentCard);
    }
	
	// converts card index and color to Card object
	public Card convertToCard(int card, int color)
    {
		switch (color)
		{
		case 0:
			switch (card)
			{
			case 0: return new Card(11, "Ace of Hearts");
			case 1: return new Card(2, "Two of Hearts");
    		case 2: return new Card(3, "Three of Hearts");
        	case 3: return new Card(4, "Four of Hearts");
        	case 4: return new Card(5, "Five of Hearts");
        	case 5: return new Card(6, "Six of Hearts");
        	case 6: return new Card(7, "Seven of Hearts");
        	case 7: return new Card(8, "Eight of Hearts");
        	case 8: return new Card(9, "Nine of Hearts");
        	case 9: return new Card(10, "Ten of Hearts");
        	case 10: return new Card(10, "Jack of Hearts");
        	case 11: return new Card(10, "Queen of Hearts");
        	case 12: return new Card(10, "King of Hearts");
        	default: return null;
        	}
		case 1:
			switch (card)
			{
			case 0: return new Card(11, "Ace of Diamonds");
			case 1: return new Card(2, "Two of Diamonds");
    		case 2: return new Card(3, "Three of Diamonds");
        	case 3: return new Card(4, "Four of Diamonds");
        	case 4: return new Card(5, "Five of Diamonds");
        	case 5: return new Card(6, "Six of Diamonds");
        	case 6: return new Card(7, "Seven of Diamonds");
        	case 7: return new Card(8, "Eight of Diamonds");
        	case 8: return new Card(9, "Nine of Diamonds");
        	case 9: return new Card(10, "Ten of Diamonds");
        	case 10: return new Card(10, "Jack of Diamonds");
        	case 11: return new Card(10, "Queen of Diamonds");
        	case 12: return new Card(10, "King of Diamonds");
        	default: return null;
        	}
		case 2:
			switch (card)
			{
			case 0: return new Card(11, "Ace of Spades");
			case 1: return new Card(2, "Two of Spades");
    		case 2: return new Card(3, "Three of Spades");
        	case 3: return new Card(4, "Four of Spades");
        	case 4: return new Card(5, "Five of Spades");
        	case 5: return new Card(6, "Six of Spades");
        	case 6: return new Card(7, "Seven of Spades");
        	case 7: return new Card(8, "Eight of Spades");
        	case 8: return new Card(9, "Nine of Spades");
        	case 9: return new Card(10, "Ten of Spades");
        	case 10: return new Card(10, "Jack of Spades");
        	case 11: return new Card(10, "Queen of Spades");
        	case 12: return new Card(10, "King of Spades");
        	default: return null;
        	}
		case 3:
			switch (card)
			{
			case 0: return new Card(11, "Ace of Clubs");
			case 1: return new Card(2, "Two of Clubs");
    		case 2: return new Card(3, "Three of Clubs");
        	case 3: return new Card(4, "Four of Clubs");
        	case 4: return new Card(5, "Five of Clubs");
        	case 5: return new Card(6, "Six of Clubs");
        	case 6: return new Card(7, "Seven of Clubs");
        	case 7: return new Card(8, "Eight of Clubs");
        	case 8: return new Card(9, "Nine of Clubs");
        	case 9: return new Card(10, "Ten of Clubs");
        	case 10: return new Card(10, "Jack of Clubs");
        	case 11: return new Card(10, "Queen of Clubs");
        	case 12: return new Card(10, "King of Clubs");
        	default: return null;
        	}
		default:
			return null;
		}
    }
	
	public int getDeck(int color, int card)
	{
		return deck[color][card];
	}

	public void setDeck(int[][] deck)
	{
		this.deck = deck;
	}

	public void deleteCard(Card card)
    {
        // the card passed in will be set null, and picked up by the garbage collector
        card = null;
    }
}
